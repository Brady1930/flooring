<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* ----------------------------------------------------------------------------
 * Mod for notes by Jared
 * ---------------------------------------------------------------------------- */

/**
 * Notes Model
 *
 * @package Models
 */
class Notes_Model extends CI_Model {
    /**
     * Add a note to the database.
     *
     * This method adds a customer to the database. If the customer
     * doesn't exists it is going to be inserted, otherwise the
     * record is going to be updated.
     *
     * @param array $customer Associative array with the customer's
     * data. Each key has the same name with the database fields.
     * @return int Returns the customer id.
     */
    public function add($note) {
        // Validate the customer data before doing anything.
        $this->validate($note);

        // :: INSERT OR UPDATE CUSTOMER RECORD
        $note['id'] = $this->_insert($note);

        return $customer['id'];
    }

    /**
     * Insert a new note into the database.
     *
     */
    protected function _insert($note) {
    
        if (!$this->db->insert('user_notes', $note)) {
            throw new Exception('Could not insert note into the database.');
        }

        return intval($this->db->insert_id());
    }

    /**
     * Validate note data before the insert or update operation is executed.
     *
     * @param array $customer Contains the customer data.
     * @return bool Returns the validation result.
     */
    public function validate($note) {
        $this->load->helper('data_validation');

        // If a customer id is provided, check whether the record
        // exist in the database.
        if (isset($notes['id'])) {
            $num_rows = $this->db->get_where('ea_users',
                    array('id' => $notes['id']))->num_rows();
            if ($num_rows == 0) {
                throw new Exception('Provided customer id does not '
                        . 'exist in the database.');
            }
        }
        // Validate required fields
        if (!isset($notes['id'])
                || !isset($notes['note'])) {
            throw new Exception('Not all required fields are provided: '
                    . print_r($notes, TRUE));
        }

        return TRUE;
    }

    /**
     * Get all, or specific records from appointment's table.
     *
     * @example $this->Model->getBatch('id = ' . $recordId);
     *
     * @param string $whereClause (OPTIONAL) The WHERE clause of
     * the query to be executed. DO NOT INCLUDE 'WHERE' KEYWORD.
     * @return array Returns the rows from the database.
     */
    public function get_batch($where_clause = '') {
        $customers_role_id = $this->get_customers_role_id();

        if ($where_clause != '') {
            $this->db->where($where_clause);
        }

        $this->db->where('id_roles', $customers_role_id);

        return $this->db->get('ea_users')->result_array();
    }

    /**
     * Get the customers role id from the database.
     *
     * @return int Returns the role id for the customer records.
     */
    public function get_customers_role_id() {
        return $this->db->get_where('ea_roles', array('slug' => DB_SLUG_CUSTOMER))->row()->id;
    }
}

/* End of file customers_model.php */
/* Location: ./application/models/customers_model.php */
